<?php


require_once __DIR__ . '/SQLite/SQLiteConnection.php';
require_once __DIR__ . '/SQLite/CreateTables.php';

// Nur zum Anlegen der Datenbank
echo "<h3>SQLITE</h3>";

 //sqLite DB wird angelgt
 $pdo = (new SQLiteConnection())->connect();

 $createTabeles = new CreateTables($pdo);

 $createTabeles->createTables();

echo "<h3>SQLITE done create Table ... </h3>";

