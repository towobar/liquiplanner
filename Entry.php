<?php
/**
 * Created by PhpStorm.
 * User: tombar
 * Date: 22.02.2015
 * Time: 18:08
 */

class Entry {


    private   $_titel = '';
    private   $_betrag = '';
    private   $_typ = '';
    private   $_datum = '';
    private   $_tstamp = '';


    public function __construct(array $daten = array())
    {
        $this->setDaten($daten);
    }

    public function setDaten(array $daten)
    {
        // wenn $daten nicht leer ist, rufe die passenden Setter auf
        if ($daten) {

            foreach ($daten as $k => $v) {

                $setterName = 'set' . ucfirst($k);

                // pruefe ob ein passender Setter existiert
                if (method_exists($this, $setterName)) {

                    $this->$setterName($v); // Setteraufruf
                }
            }
        }
    }






    public function setTitel($titel)
    {

        $this->_titel = $titel;

    }

    public function setBetrag($betrag)
    {

        $this->_betrag = $betrag;

    }

    public function setTyp($typ)
    {

        $this->_typ = $typ;

    }

    public function setDatum($datum)
    {

        $this->_datum = $datum;

    }

    public function setTstamp($tstamp)
    {

        $this->_tstamp = $tstamp;

    }




}