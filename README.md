# README #

## This is a demo application. The app was created during a javascript course. Using the example of a budget book (Liquidplaner), the structure and design of a modern modern javascript app is demonstrated.
                                                                             
* * * 
### What is this repository for? ###

* The application uses modern, current javascript standards which are based on ES6 and ES10. The app is designed to be fully object-oriented.
* The app consists of individual classes. These classes will be provided as javascript modules.
* These modules are chained together in the parent module : liqui-planner.js . In the index.php only this parent module is included.
*  The app is a pure javascript implementation. All HTML elements are generated and displayed using the 
   javascript Document Object Model(DOM) methods.
*   The status ( all entries ) of the Liquidplanner is stored as a json string.
    The status can then be stored in the session storage of the browser or
    via the storage button into a mysql database.
*   Saving to the database
    is realized via an ajax call and a php DB class.
    
    ![alternativetext](src/img/frontend1.png)