<?php


class ManageDB
{

    /**
     * PDO object
     * @var \PDO
     */
    private $pdo;

    /**
     * connect to the SQLite database
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }


    /**
     * @param $json_entries
     * @return bool
     */
    public function insert_data_as_json($json_entries): bool
    {
        $success = true;

        try {

            $sqlString = "INSERT INTO json_data(js_string)
                      VALUES (:js_string)";

            $dbInsert = $this->pdo->prepare($sqlString);
            $data = array(':js_string' => $json_entries);

            $dbInsert->execute($data);

        } catch (PDOException $e) {

            $success = false;
        }

        return $success;

    }

    /**
     * @param $json_entries
     * @return bool
     */
    public function update_data_as_json($json_entries): bool
    {
        $success = true;

       // $sqlString = "UPDATE  json_data SET js_string = :js_string  order by id desc limit 1";

        // Es gibt nur ein Eintrag in der tabelle der immer überschrieben wird !
        $sqlString = "UPDATE  json_data SET js_string = :js_string  where id = 1";

        try
        {

            $dbUpdate  =  $this->pdo->prepare($sqlString);

            $data = array(':js_string' => $json_entries);

            $success = $dbUpdate->execute($data);


        } catch (PDOException $e) {

            $success = false;
        }

        return $success;

    }

    /**
     * @return mixed|null
     */
    public function get_last_entries_as_json()
    {
        $result = null;

        try {

            $sqlString = "select js_string from json_data order by id desc limit 1";

            $dbSelect = $this->pdo->prepare($sqlString);

            if ($dbSelect->execute()) {
                $result = $dbSelect->fetchColumn();
            }


        } catch (PDOException $e) {

            $result = null;
        }


        return $result;

    }

}