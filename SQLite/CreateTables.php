<?php


class CreateTables
{

    /**
     * PDO object
     * @var \PDO
     */
    private $pdo;

    /**
     * connect to the SQLite database
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * create tables
     */
    public function createTables(): void
    {
        $commands = ['CREATE TABLE IF NOT EXISTS json_data (
                        id   INTEGER PRIMARY KEY,
                        js_string TEXT NOT NULL
                      )'
        ];

        // execute the sql commands to create new tables
        foreach ($commands as $command) {
            $this->pdo->exec($command);
        }
    }




}