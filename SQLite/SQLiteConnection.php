<?php


require_once __DIR__ . '/Config.php';



class SQLiteConnection
{

    /**
     * PDO instance
     * @var PDO $pdo
     */
    private  $pdo;

    /**
     * return in instance of the PDO object that connects to the SQLite database
     * @return PDO
     */
    public function connect(): PDO
    {
        if ($this->pdo === null) {
            try {
                $this->pdo = new PDO("sqlite:" . Config::PATH_TO_SQLITE_FILE);
                //  Importend !! for Exeption-Handling
                $this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            } catch (\PDOException $exception) {
                echo $exception->getMessage();
            }

        }
        return $this->pdo;
    }

}