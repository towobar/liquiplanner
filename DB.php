<?php
require_once __DIR__ . '/Entry.php';

class DB
{

    private $dB = null;


    private const DB_HOST = 'localhost';
    private const DB_PORT = '3306';
    private const DB_USER = 'root';
    private const DB_PASS = '';
    private const DB_NAME = 'liqui_planner';


    public function __construct()
    {

        $dsn = 'mysql:host=' . self::DB_HOST . ';port=' . self::DB_PORT . ';dbname=' . self::DB_NAME;

        try {
            $this->dB = new PDO($dsn, self::DB_USER, self::DB_PASS, array(

                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
            ));

            // Damit � � etc. richtig dargestellt werden !
            $this->dB->exec('SET NAMES utf8');


        } catch (Exception $e) {
            // If the DB connection fails, output the error
            die ($e->getMessage());

        }

    }

    /**
     * @return PDO|null
     */
    public function getPDO()
    {
        return $this->dB;

    }

    /**
     * @param $json_entries
     * @return bool
     */
    public function insert_data_as_json($json_entries): bool
    {
        $success = true;

        try {

            $sqlString = "INSERT INTO json_data(js_string)
                      VALUES (:js_string)";

            $dbInsert = $this->dB->prepare($sqlString);
            $data = array(':js_string' => $json_entries);

            $dbInsert->execute($data);

        } catch (PDOException $e) {

            $success = false;
        }

        return $success;

    }

    /**
     * @param $json_entries
     * @return bool
     */
    public function update_data_as_json($json_entries): bool
    {
        $success = true;

        $sqlString = "UPDATE  json_data SET js_string = :js_string  order by id desc limit 1";

        try
        {

            $dbUpdate  =  $this->dB->prepare($sqlString);

            $data = array(':js_string' => $json_entries);

            $success = $dbUpdate->execute($data);


        } catch (PDOException $e) {

            $success = false;
        }

        return $success;

    }


    /**
     * @param $entry
     * @return bool
     */
    public function insert_entry($entry): bool
    {
        $success = true;

        try {

            $sqlString = "INSERT INTO entries (titel,betrag,typ,datum,tstamp)
                      VALUES (:titel,:betrag,:typ,:datum,:tstamp)";

            $dbInsert = $this->dB->prepare($sqlString);
            $data = array(
                ':titel' => $entry->_titel,
                ':betrag' => $entry->_betrag,
                ':typ' => $entry->_typ,
                ':datum' => $entry->_datum,
                ':tstamp' => $entry->_timestamp

            );

            $dbInsert->execute($data);

        } catch (PDOException $e) {

            $success = false;
        }

        return $success;

    }

    /**
     * @return mixed|null
     */
    public function get_last_entries_as_json()
    {
        $result = null;

        try {

            $sqlString = "select js_string from json_data order by id desc limit 1";

            $dbSelect = $this->dB->prepare($sqlString);

            if ($dbSelect->execute()) {
                $result = $dbSelect->fetchColumn();
            }


        } catch (PDOException $e) {

            $result = null;
        }


        return $result;

    }

    /**
     * Gibt die entries als object array zur�ck
     * @return array
     */
    public function get_entries()
    {

        $entries = [];

        try {

            $sqlString = "select * from entries";

            $dbSelect = $this->dB->prepare($sqlString);

            if ($dbSelect->execute()) {
                while ($row = $dbSelect->fetch()) {

                    $entries[] = new Entry($row);
                }
            }

        } catch (PDOException $e) {

            $entries[] = null;
        }


        return $entries;

    }

}