<?php

require_once __DIR__ . '/DB.php';
require_once __DIR__ . '/SQLite/SQLiteConnection.php';
require_once __DIR__ . '/SQLite/ManageDB.php';

// entries als json aus MariaDB
if(isset($_POST['loadEntries'])) {

    // sqLite DB
   // $pdo = (new SQLiteConnection())->connect();

    // Maria SQLDB
    $dB = new DB();

    $jsonResult = $dB->get_last_entries_as_json();

    // entries als object array
    //  $entriesResult = $dB->get_entries();

    echo $jsonResult;
    exit;

}

// entries als json aus SQLite DB
if(isset($_POST['loadEntriesSQLite'])) {


    // sqLite DB
    $pdo = (new SQLiteConnection())->connect();

    $dB = new ManageDB($pdo);

    $jsonResult = $dB->get_last_entries_as_json();

    // entries als object array
    //  $entriesResult = $dB->get_entries();

    echo $jsonResult;
    exit;

}





// Alle Eintraege in die MariaDB speichern
if (isset($_POST['entries'])) {


    $json_entries = $_POST['entries'];

    $dB = new DB();

    // entries als json string in die json_data Tabelle
    if ($dB->update_data_as_json($json_entries))
    {
        $success =  'ok';
    }

    // entries als einzelne Reihen in die entries Tabelle
    $entries = json_decode($json_entries,false);

    foreach ($entries as $entry)
    {
      $success =   $dB->insert_entry($entry);
    }

    echo $success ? 'ok': 'error';

    exit;
}

// Alle Eintraege in die SQLiteDB speichern
if (isset($_POST['entriesSQLite'])) {


    $json_entries = $_POST['entriesSQLite'];

    // sqLite DB
    $pdo = (new SQLiteConnection())->connect();


    $dB = new ManageDB($pdo);

    // entries als json string in die json_data Tabelle
    if ($dB->update_data_as_json($json_entries))
    {
        $success =  true;
    }



    echo $success ? 'ok': 'error';

    exit;
}
